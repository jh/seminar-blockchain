# LaTeX Beamer

Theme: [metropolis](https://github.com/matze/mtheme)

Theme Demo: https://github.com/matze/mtheme/tree/master/demo ([PDF](http://mirrors.ctan.org/macros/latex/contrib/beamer-contrib/themes/metropolis/demo/demo.pdf))

Theme Documentation: http://mirrors.ctan.org/macros/latex/contrib/beamer-contrib/themes/metropolis/doc/metropolistheme.pdf

BibLaTeX Overview: https://en.wikibooks.org/wiki/LaTeX/Bibliography_Management

BibLaTeX Documentation: http://mirrors.ctan.org/macros/latex/contrib/biblatex/doc/biblatex.pdf

Packages: `texlive-latex-extra texlive-fonts-extra biber`
