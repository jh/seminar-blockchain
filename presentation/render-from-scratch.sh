#!/bin/bash

NAME=presentation

rm -f *.aux *.bbl *.blg *.log *.nav *.out *.pdf *.snm *.toc *.vrb

lualatex "$NAME"
biber "$NAME"
lualatex "$NAME"
lualatex "$NAME"
