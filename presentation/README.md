# Quantitative Aspects of the Blockchain: PoW, its Energy Usage and alternative Consensus Systems

## Outline

## Introduction
Why is a consensus required?
-> *Trust*

### Byzantin Generals Problem
Communication over an unreliable (i.e. untrusted) channel


## Bitcoin Proof-of-Work

Proof of Work analogy: Telephone book
It is hard (but entirely possible) to find a name in the telephone book knowing only the telephone number (or a part of it). However, once the work has been done, it is verifiable by other, by simply looking up the name (via the telephone index).

This is one of the key features of PoW schemes: asymmetry.

### Types of Proves

Challenge response:
1. Alice requests a service from Bob
2. Bob selects/generates a challenge for Alice
3. Alice searches for an answer to the challenge (requires some kind of resource)
4. Alice sends the solution to Bob
5. Bob verifies the solution by Alice (simple/quick)
7. Bob grants Alice access to the service

-> requires direct communication between Alice and Bob

Solution verification:
1. Alice chooses a problem
2. Alice solves the problem (requires some kind of resource)
3. Alice publishes her problem along with the result
4. Bob obtains Alice's problem and result
5. Bob check the problem choice and verifies the result

-> Alice and Bob need to agree on the same kind of problem
[Altcoin forks]

### What are miners doing?

1. Connect to other nodes
2. Download and verify entire blockchain (why?)
3. Verify incoming transactions
4. Create block with received transactions
5. Find nonce for valid block header (referred to as "mining")
6. Share new block with other nodes
7. `goto 4`

The steps to run the network are as follows:
1) New transactions are broadcast to all nodes.
2) Each node collects new transactions into a block.
3) Each node works on finding a difficult proof-of-work for its block.
4) When a node finds a proof-of-work, it broadcasts the block to all nodes.
5) Nodes accept the block only if all transactions in it are valid and not already spent.
6) Nodes express their acceptance of the block by working on creating the next block in the chain, using the hash of the accepted block as the previous hash.

Nodes always consider the longest chain to be the correct one and will keep working on extending it. If two nodes broadcast different versions of the next block simultaneously, some nodes may receive one or the other first. In that case, they work on the first one they received, but save the other branch in case it becomes longer. The tie will be broken when the next proof-of-work is found and one branch becomes longer; the nodes that were working on the other branch will then switch to the longer one.

-- Satoshi Nakamoto Bitcoin whitepaper

### Mining
Hash puzzle

### Why are they doing it?

Bitcoin Business 101:
Profit = reward - cost
Reward = block reward + transaction fees
cost = hardware cost + operating costs
=> profit = block reward + transaction fees - hardware cost - operating costs >>^! 0

### Block Difficulty
Analogy: rubber banding effect

### Hardware [Cost]
CPU -> GPU -> FPGA -> ASIC

Bitmain Antminer S9 Specs:
* Hash Rate: 13.5 TH/s
* Power Consumption: 1300W
* Power Efficiency: 0.098 J/GH (most efficien ASIC miner to date)
* 16nm lithography process
* Price: 1500€
[pics here]

### Operating [Costs]

* Energy Costs (Electricity)
* Cooling [probably also electricity, unless Iceland]

Contradiction? Wasteful? Heating and Cooling

Sidetrack:
### Proof Of Useful Work
Option a) Use heat for something else! E.g. heating houses, water, etc. => data furnace
[pic of MS research here]
=> most efficient heater ever, turns electricity into money AND heat.
Can be combined with spikes of renewable energy, to prevent energy grid from damage (demand must meet supply).
[not very sustainable]

Option b) Use proof-of-work for something useful!

"Many POW systems require the clients to do useless work, such as inverting a hash function. This means that a lot of resources (mainly the electricity that powers the clients' computers) is wasted in vain. To mitigate the loss, some alternative coins use a POW system where the performed work is actually useful. For example, Primecoin requires clients to find unknown prime numbers of certain types, which can have useful side-applications."

* Finding prime numbers: "Primecoin network searches for special prime number chains known as Cunningham chains and bi-twin chains." [primecoin]
* Solarcoin https://solarcoin.org/en/frequently-asked-questions
* Gridcoin https://www.gridcoin.us/ (ALIENS SETI http://setiathome.ssl.berkeley.edu/)

=> problems: exhaustable [not enough data], not equiprobable [solutions are not equally likely], centralized authority [need for POW? / distributed ledger / blockchain]


### Mining Pools
Why?
Only one up-to-date and verified blockchain per pool required

Mining Pool Distribution (Geographic & Percentage)

## Alternativ Consensus [Method/Algorithm/Techniques]

https://www.coindesk.com/short-guide-blockchain-consensus-protocols/

https://blockgeeks.com/guides/proof-of-work-vs-proof-of-stake/

### Proof-of-Stake
Used resource: native currency

Two implementations:

#### Tendermint
Used in blockchain network Cosmos

Byzantin fault tolerance-based PoS:
* weakly asynchronous (only a single synchronous state for a maximum amount of time)
* favors consistency over availability (requires only 1/3 of "evil" votes to halt, but at least 2/3 of votes to fail)
* energy efficient & fast
* well suited for enterprise blockchain

http://tendermint.readthedocs.io/en/master/introduction.html

#### Casper
Hybrid PoW-PoS (planned: gentle transition to pure PoS)

Idea: Anyone can bond tokens (coins), decision leading to a convergence on the chain make money, otherwise lose money

To be used in Ethereum

Problem with PoS: large coin holders can stake more, smaller coin holders get less rewards
-> the rich get gradually richer

Mitigation:
### Delegated Proof of Stake (DPoS)
Alice (and possibly others) delegates the weight of her tokens to Bob.
Then Bob is supposed to vote on behalf of Alice with her tokens.
Small groups can aggregate tokens and therefore get a *proportional* voice in the network
Similar to US' Electoral College system.

### Proof-of-Burn

[apply water to burnt area here]

Idea: send coins to an address where they can never be retrieved from ("lost"), and get proportinate votes on the blockchain for the "spent" coins
Consumed resource: currency

Used by Slimcoin

Can be used for bootstrapping a cryptocurrency:
Users who "burn" the original coin (e.g. Bitcoin) are awarded with coins in the new currency (Altcoin).



### Proof of Storage / Proof of Space
Consumed resource: memory

Disk space is challenge here
"Miners" are rewarded with coins for storing information

Permacoin: stores very large files, each miner hosts a little block ???
Filecoin
Sia
Storj.io

Provable Data Possession (PDP)

Prove-of-Retrievability (PoR)

Prove-of-Replication (PoRep)

Prove-of-Spacetime (PoSt)
// see filecoin paper

### Proof-of-Authority
Not applicable for public, distributed blockchains
Requires trusted network (e.g. cloud infrastructure)

### Proof of Luck
https://youtu.be/RgPIdGJHsfE

## Voting based consensus
Paxos

Raft

### Other links
https://news.ycombinator.com/item?id=6911785

https://www.mail-archive.com/cryptography@metzdowd.com/msg09997.html

http://paulbohm.com/articles/bitcoins-value-is-decentralization/

http://nonchalantrepreneur.com/post/70130104170/bitcoin-and-the-byzantine-generals-problem

https://en.bitcoin.it/wiki/Difficulty
