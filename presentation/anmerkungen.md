# Anmerkungen zur Präsentation am 2017-12-14

```diff
- zu wenig Pausen
- Laustärke zu leise (?)
+ sehr locker, sprachlich gut
- teilweise recht schnell durch Folien gezappt
- meistens zu viel Text auf Folien
+ angenehm amüsant
- übertriebene Gesten beim Vorlesen von Text (?)
+ sehr motiviert und sympathisch vorgetragen
+ Englisch gut (flüssig)
- Aussprache teilweise schwer verständlich (Nuscheln)
+ Überdurchschnittlich guter roter Faden
- Raumverhalten zu aktiv -> bei Nervosität nicht auf der Stelle laufen (?)
- weniger "Wall Of Text", weniger ablesen
- zu viele Jokes oder Lächerlichkeiten, sowas hat nichts in einer wissenschaftlichen Arbeit/Präsentation zu suchen (?)
- Sprechtempo zu schnell
```
