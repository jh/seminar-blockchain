# Quantitative Aspekte der Blockchain: Energiebedarf bei PoW und alternative Konsensverfahren im Vergleich

https://www.youtube.com/watch?v=XH1Vog9mg4M&list=PLSONl1AVlZNUuLCzR2F5hI4Pi-11wJYpY

## Introduction
PoW: Proof of Work

Why do we need it?
-> The Byzantine Generals Problem
https://www.mtv.tu-berlin.de/fileadmin/a3435/Lehre/SoSe06/DistAlgs/Byzanz1.pdf

BFT https://en.wikipedia.org/wiki/Byzantine_fault_tolerance

What is it?

----

https://bitcoin.org/en/faq

### What is Bitcoin?

Bitcoin is a *consensus network* that enables a new payment system and a completely digital money. It is the first decentralized peer-to-peer payment network that is powered by its users with *no central authority or middlemen*.

### Why do people trust Bitcoin?

Much of the trust in Bitcoin comes from the fact that it requires no trust at all. Bitcoin is fully open-source and decentralized. This means that anyone has access to the entire source code at any time. Any developer in the world can therefore verify exactly how Bitcoin works. All transactions and bitcoins issued into existence can be transparently consulted in real-time by anyone. All payments can be made without reliance on a third party and the whole system is protected by heavily peer-reviewed cryptographic algorithms like those used for online banking. *No organization or individual can control Bitcoin, and the network remains secure even if not all of its users can be trusted.*

## Isn't Bitcoin mining a waste of energy?

*Spending energy to secure and operate a payment system is hardly a waste*. Like any other payment service, the use of Bitcoin entails processing costs. Services necessary for the operation of currently widespread monetary systems, such as banks, credit cards, and armored vehicles, also use a lot of energy. *Although unlike Bitcoin, their total energy consumption is not transparent and cannot be as easily measured.*

Bitcoin mining has been designed to become more optimized over time with specialized hardware consuming less energy, and the operating costs of mining should continue to be proportional to demand. When Bitcoin mining becomes too competitive and less profitable, some miners choose to stop their activities. Furthermore, all energy expended mining is eventually transformed into heat, and the most profitable miners will be those who have put this heat to good use. An optimally efficient mining network is one that isn't actually consuming any extra energy. While this is an ideal, the economics of mining are such that miners individually strive toward it.

## How does mining help secure Bitcoin?

Mining creates the equivalent of a competitive lottery that makes it very difficult for anyone to consecutively add new blocks of transactions into the block chain. This protects the neutrality of the network by preventing any individual from gaining the power to block certain transactions. This also prevents any individual from replacing parts of the block chain to roll back their own spends, which could be used to defraud other users. Mining makes it exponentially more difficult to reverse a past transaction by requiring the rewriting of all blocks following this transaction.

## Mining

*Bitcoin mining is the process of making computer hardware do mathematical calculations for the Bitcoin network to confirm transactions and increase security. As a reward for their services, Bitcoin miners can collect transaction fees for the transactions they confirm, along with newly created bitcoins.* Mining is a specialized and competitive market where the rewards are divided up according to how much calculation is done. Not all Bitcoin users do Bitcoin mining, and it is not an easy way to make money.

----

http://www.reuters.com/article/us-markets-bitcoin-mining/bitcoin-miners-face-fight-for-survival-as-new-supply-halves-idUSKCN0ZO2CW

The process has come to be known as “mining” because it is slow and intensive, reaping a gradual reward in the same way that minerals such as gold are mined from the ground.


Streng reckons that, on average, it costs about $200 in electricity, including cooling power, to mine one bitcoin. Equipment, rent, wages and business running costs are on top.

It is no coincidence that so many mining companies have chosen to build farms in Iceland - Chinese giant Bitmain also has a huge farm there. The volcanic island’s cheap, bountiful, renewable energy supply, good internet connectivity, and cool temperatures make it an ideal location.

The Icelandic authorities welcome the boost to the economy that the bitcoin miners have brought -- Bitmain opened its farm after an approach by the Icelandic embassy in Beijing. Genesis’s Streng says he is such a valued client that the Icelandic energy companies fly him around in helicopters.

2016-07-08

----

https://www.cryptocoinsnews.com/as-mining-expands-will-electricity-consumption-constrain-bitcoin/

China has the largest number of datacenter mines. The largest share of Chinese mines are close to Tibet in an area with abundant, cheap hydropower. Datacenter mines also exist in Iceland, Malaysia, Venezuela, the Republic of Georgia and other countries.

A senior executive at a mining firm who did not want to be named estimated global bitcoin mining power at 600 megawatts. This is based on the number of calculations being performed and the average energy efficiency involved. That number is constantly improving.

The bitcoin executive estimated the industry spends $250 million yearly on electricity. Added to capital costs, the annual cost to operate the network is between $400 million and $500 million. Compared to around $525 million in yearly revenue (1.4 million bitcoins released at an average price of $375), the margins are not high.

Nevertheless, the efficiencies of bitcoin bode well for its future viability, Kelly-Detwiler noted. The cumulative value of bitcoin transactions in 2015 was slightly above $60 billion at a system network cost of half a billion dollars

PayPal, by contrast, reported $2 billion in expenses for 1.4 billion transactions totaling $81 billion in payments. Bitcoin is more efficient when considering the value of payments against costs. The comparison is not perfect, Kelly-Detwiler noted, but it highlights bitcoin’s inherent efficiencies.

2016-07-25

----

https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2801048

Bitcoin Mining Proof of Work Costs: Large, Wasteful but Fair

----

https://digiconomist.net/bitcoin-energy-consumption

see "Recommended Reading"

----

https://en.wikipedia.org/wiki/Bitcoin_network#Mining

To form a distributed timestamp server as a peer-to-peer network, bitcoin uses a proof-of-work system. *This work is often called bitcoin mining*. The signature is discovered rather than provided by knowledge. This process is energy intensive. Electricity can consume more than 90% of operating costs for miners. A data center in China, planned mostly for bitcoin mining, is expected to require up to 135 megawatts of power.

*Requiring a proof of work to provide the signature for the blockchain was Satoshi Nakamoto's key innovation.* [5] The mining process involves identifying a block that, when hashed twice with SHA-256, yields a number smaller than the given difficulty target. While the average work required increases in inverse proportion to the difficulty target, a hash can always be verified by executing a single round of double SHA-256.

For the bitcoin timestamp network, a valid proof of work is found by incrementing a nonce until a value is found that gives the block's hash the required number of leading zero bits. Once the hashing has produced a valid result, the block cannot be changed without redoing the work. As later blocks are chained after it, the work to change the block would include redoing the work for each subsequent block.

Majority consensus in bitcoin is represented by the longest chain, which required the greatest amount of effort to produce. If a majority of computing power is controlled by honest nodes, the honest chain will grow fastest and outpace any competing chains. To modify a past block, an attacker would have to redo the proof-of-work of that block and all blocks after it and then surpass the work of the honest nodes. The probability of a slower attacker catching up diminishes exponentially as subsequent blocks are added.[2]
Mining difficulty has increased significantly

To compensate for increasing hardware speed and varying interest in running nodes over time, the difficulty of finding a valid hash is adjusted roughly every two weeks. If blocks are generated too quickly, the difficulty increases and more hashes are required to make a block and to generate new bitcoins.

----

„Proof-of-Authority basiert auf einem Diktatorknoten, der vorgibt, was richtig ist. Sehr schlicht, sehr einfach, sehr effizient.“
Christoph Jentzsch (Slock.it)

BDEW Blockchain in der Energiewirtschaft

----

"At its most basic, the blockchain is global spreadsheet -- an incorruptible digital ledger of economic transactions that can be programmed to record not just financial transactions but virtually everything of value.“
Don Tapscott

What’s the Next Generation Internet? Surprise: It’s all about the Blockchain!

----

https://en.wikipedia.org/wiki/Proof-of-work_system

List of proof-of-work functions

Here is a list of known proof-of-work functions:

    Integer square root modulo a large prime[1]
    Weaken Fiat–Shamir signatures[1]
    Ong–Schnorr–Shamir signature broken by Pollard[1]
    Partial hash inversion[11][12][2] This paper formalizes the idea of a proof of work (POW) and introduces "the dependent idea of a bread pudding protocol", a "re-usable proof of work" (RPOW) system.[13] as Hashcash
    Hash sequences[14]
    Puzzles[15]
    Diffie–Hellman-based puzzle[16]
    Moderate[6]
    Mbound[7]
    Hokkaido[8]
    Cuckoo Cycle[9]
    Merkle tree based[17]
    Guided tour puzzle protocol[10]

----

https://bitcoin.org/en/developer-reference#block-chain

https://en.wikipedia.org/wiki/Bitcoin_network#Mining

----


# Blockchain Stats
https://blockchain.info/de

https://etherscan.io/

https://ethstats.net/


----

# Comparison Charts
https://bitinfocharts.com/comparison/transactions-btc-ltc-nmc.html

https://bitcoinwisdom.com/bitcoin/difficulty

----

# Calculators / Calculations

https://news.ycombinator.com/item?id=15759468

https://powercompare.co.uk/bitcoin/

https://bitcoinwisdom.com/bitcoin/calculator

https://en.bitcoin.it/wiki/Generation_Calculator


https://www.coindesk.com/microscope-economic-environmental-costs-bitcoin-mining/ :

With a network hashrate of 110 million GH/s, the network needs 0.7333 x 110 million Watts = 80,666 kW. This equates to 80,666 kW x 24 hrs/day x 365.25 days/yr = 707,120,500 kWh/year.

This equates to 2.54 million GJ/year, and 424,725 tonnes of CO2/year.

At $100/MWh, this electricity would cost $70,712,000/year.

----

Bitcoin Colors: http://www.color-hex.com/color-palette/4955

Bitcoin Logo: https://en.bitcoin.it/wiki/Promotional_graphics
