Title: ETHEREUM: A SECURE DECENTRALISED GENERALISED TRANSACTION LEDGER

Author: Dr. Gavin Wood

Retrieved: 2017-11-25

Source: https://ethereum.github.io/yellowpaper/paper.pdf
