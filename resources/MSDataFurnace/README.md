Title: The Data Furnace: Heating Up with Cloud Computing

Author: Jie Liu, Michel Goraczko, Sean James, Christian Belady, Jiakang Lu, Kamin Whitehouse

Retrieved: 2017-11-25

Source: https://www.microsoft.com/en-us/research/publication/the-data-furnace-heating-up-with-cloud-computing/
