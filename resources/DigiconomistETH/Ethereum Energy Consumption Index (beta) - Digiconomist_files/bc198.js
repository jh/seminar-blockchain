jQuery.fn.exists = function(callback) {
  var args = [].slice.call(arguments, 1);
  if (this.length) {
	callback.call(this, args);
  }
  return this;
};

/*----------------------------------------------------
/* Show/hide Scroll to top
/*--------------------------------------------------*/
jQuery(document).ready(function($) {
	//move-to-top arrow
	jQuery("body").prepend("<a id='move-to-top' class='animate ' href='#blog'><i class='fa fa-chevron-up'></i></a>");
	
	var scrollDes = 'html,body';  
	/*Opera does a strange thing if we use 'html' and 'body' together so my solution is to do the UA sniffing thing*/
	if(navigator.userAgent.match(/opera/i)){
		scrollDes = 'html';
	}
	//show ,hide
	jQuery(window).scroll(function () {
		if (jQuery(this).scrollTop() > 160) {
			jQuery('#move-to-top').addClass('filling').removeClass('hiding');
		} else {
			jQuery('#move-to-top').removeClass('filling').addClass('hiding');
		}
	});
});


/*----------------------------------------------------
/* Make all anchor links smooth scrolling
/*--------------------------------------------------*/
jQuery(document).ready(function($) {
 // scroll handler
  var scrollToAnchor = function( id, event ) {
	// grab the element to scroll to based on the name
	var elem = $("a[name='"+ id +"']");
	// if that didn't work, look for an element with our ID
	if ( typeof( elem.offset() ) === "undefined" ) {
	  elem = $("#"+id);
	}
	// if the destination element exists
	if ( typeof( elem.offset() ) !== "undefined" ) {
	  // cancel default event propagation
	  event.preventDefault();

	  // do the scroll
	  // also hide mobile menu
	  var scroll_to = elem.offset().top;
	  $('html, body').removeClass('mobile-menu-active').animate({
			  scrollTop: scroll_to
	  }, 600, 'swing', function() { if (scroll_to > 46) window.location.hash = id; } );
	}
  };
  // bind to click event
  $("a").click(function( event ) {
	// only do this if it's an anchor link
	var href = $(this).attr("href");
	if ( href && href.match("#") && href !== '#' ) {
	  // scroll to the location
	  var parts = href.split('#'),
		url = parts[0],
		target = parts[1];
	  if ((!url || url == window.location.href.split('#')[0]) && target)
		scrollToAnchor( target, event );
	}
  });
});

/*----------------------------------------------------
/* Responsive Navigation
/*--------------------------------------------------*/
if (mts_customscript.responsive && mts_customscript.nav_menu != 'none') {
	jQuery(document).ready(function($){
		$('.secondary-navigation').append('<div id="mobile-menu-overlay" />');
		// merge if two menus exist
		if (mts_customscript.nav_menu == 'both' && !$('.navigation.mobile-only').length) {
			$('.navigation').not('.mobile-menu-wrapper').find('.menu').clone().appendTo('.mobile-menu-wrapper').hide();
		}
	
		$('.toggle-mobile-menu').click(function(e) {
			e.preventDefault();
			e.stopPropagation();
			$('body').toggleClass('mobile-menu-active');

			if ( $('body').hasClass('mobile-menu-active') ) {
				if ( $(document).height() > $(window).height() ) {
					var scrollTop = ( $('html').scrollTop() ) ? $('html').scrollTop() : $('body').scrollTop();
					$('html').addClass('noscroll').css( 'top', -scrollTop );
				}
				$('#mobile-menu-overlay').fadeIn();
			} else {
				var scrollTop = parseInt( $('html').css('top') );
				$('html').removeClass('noscroll');
				$('html,body').scrollTop( -scrollTop );
				$('#mobile-menu-overlay').fadeOut();
			}
		});
	}).on('click', function(event) {

		var $target = jQuery(event.target);
		if ( ( $target.hasClass("fa") && $target.parent().hasClass("toggle-caret") ) ||  $target.hasClass("toggle-caret") ) {// allow clicking on menu toggles
			return;
		}
		jQuery('body').removeClass('mobile-menu-active');
		jQuery('html').removeClass('noscroll');
		jQuery('#mobile-menu-overlay').fadeOut();
	});
}

/*----------------------------------------------------
/*  Dropdown menu
/* ------------------------------------------------- */
jQuery(document).ready(function($) {
	
	function mtsDropdownMenu() {
		var wWidth = $(window).width();
		if(wWidth > 865) {
			$('.navigation ul.sub-menu, .navigation ul.children').hide();
			var timer;
			var delay = 100;
			$('.navigation li').hover( 
			  function() {
				var $this = $(this);
				timer = setTimeout(function() {
					$this.children('ul.sub-menu, ul.children').slideDown('fast');
				}, delay);
				
			  },
			  function() {
				$(this).children('ul.sub-menu, ul.children').hide();
				clearTimeout(timer);
			  }
			);
		} else {
			$('.navigation li').unbind('hover');
			$('.navigation li.active > ul.sub-menu, .navigation li.active > ul.children').show();
		}
	}

	mtsDropdownMenu();

	$(window).resize(function() {
		mtsDropdownMenu();
	});
});

/*---------------------------------------------------
/*  Vertical menus toggles
/* -------------------------------------------------*/
jQuery(document).ready(function($) {

	$('.widget_nav_menu, .navigation .menu').addClass('toggle-menu');
	$('.toggle-menu ul.sub-menu, .toggle-menu ul.children').addClass('toggle-submenu');
	$('.toggle-menu ul.sub-menu').parent().addClass('toggle-menu-item-parent');

	$('.toggle-menu .toggle-menu-item-parent').append('<span class="toggle-caret"><i class="fa fa-plus"></i></span>');

	$('.toggle-caret').click(function(e) {
		e.preventDefault();
		$(this).parent().toggleClass('active').children('.toggle-submenu').slideToggle('fast');
	});
});

/*----------------------------------------------------
/* Social button scripts
/*---------------------------------------------------*/
jQuery(document).ready(function($){
	(function(d, s) {
	  var js, fjs = d.getElementsByTagName(s)[0], load = function(url, id) {
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.src = url; js.id = id;
		fjs.parentNode.insertBefore(js, fjs);
	  };
	jQuery('span.facebookbtn, span.facebooksharebtn, .facebook_like').exists(function() {
	  load('//connect.facebook.net/en_US/all.js#xfbml=1&version=v2.3', 'fbjssdk');
	});
	jQuery('span.gplusbtn').exists(function() {
	  load('https://apis.google.com/js/plusone.js', 'gplus1js');
	});
	jQuery('span.twitterbtn').exists(function() {
	  load('//platform.twitter.com/widgets.js', 'tweetjs');
	});
	jQuery('span.linkedinbtn').exists(function() {
	  load('//platform.linkedin.com/in.js', 'linkedinjs');
	});
	jQuery('span.pinbtn').exists(function() {
	  load('//assets.pinterest.com/js/pinit.js', 'pinterestjs');
	});
	jQuery('span.stumblebtn').exists(function() {
	  load('//platform.stumbleupon.com/1/widgets.js', 'stumbleuponjs');
	});
	}(document, 'script'));
});

/*----------------------------------------------------
/* Lazy load avatars
/*---------------------------------------------------*/
jQuery(document).ready(function($){
	var lazyloadAvatar = function(){
		$('.comment-author .avatar').each(function(){
			var distanceToTop = $(this).offset().top;
			var scroll = $(window).scrollTop();
			var windowHeight = $(window).height();
			var isVisible = distanceToTop - scroll < windowHeight;
			if( isVisible ){
				var hashedUrl = $(this).attr('data-src');
				if ( hashedUrl ) {
					$(this).attr('src',hashedUrl).removeClass('loading');
				}
			}
		});
	};
	if ( $('.comment-author .avatar').length > 0 ) {
		$('.comment-author .avatar').each(function(i,el){
			$(el).attr('data-src', el.src).removeAttr('src').addClass('loading');
		});
		$(function(){
			$(window).scroll(function(){
				lazyloadAvatar();
			});
		});
		lazyloadAvatar();
	}
});

/*----------------------------------------------------
/* Grid
/*--------------------------------------------------*/
jQuery(document).ready(function($) {
jQuery('#grid').click(function() {
		jQuery(this).addClass('active');
		jQuery('#list').removeClass('active');
		jQuery('.excerpt').fadeOut(300, function() {
			jQuery(this).addClass('grid').fadeIn(300);
			jQuery.cookie('selected_layout', 'grid', { expires: 7, path: '/' });
		});
		return false;
	});
	
	jQuery('#list').click(function() {
		jQuery(this).addClass('active');
		jQuery('#grid').removeClass('active');
		jQuery('.excerpt').fadeOut(300, function() {
			jQuery(this).removeClass('grid').fadeIn(300);
			jQuery.cookie('selected_layout', 'list', { expires: 7, path: '/' });
		});
		return false;
	});
});
;/**
  * Cryptocoin Payment Box Javascript   
  *
  * @package     GoUrl Bitcoin/Altcoin Payment Box and Crypto Captcha
  * @copyright   2014-2017 Delta Consultants
  * @category    Javascript
  * @website     https://gourl.io
  * @api         https://gourl.io/api.html
  * @version     1.8.2
  *
  */

function cryptobox_custom(t,e,a,o,n){var r=(new Date).getTime(),i=((new Date).getTime(),!1),l=!1;return"number"!=typeof e&&(e=0),"string"!=typeof a&&(a=""),"string"!=typeof o&&(o="gourl_"),"string"!=typeof n&&(n=""),cryptobox_callout=function(){$.ajax({type:"GET",url:t,cache:!1,contentType:"application/json; charset=utf-8",data:{format:"json"},dataType:"json"}).fail(function(){$("."+o+"error_message").html('Error loading data ! &#160; <a target="_blank" href="'+t+'">Raw details here &#187;</a>'),$("."+o+"loader_button").fadeOut(400,function(){$("."+o+"loader").show(),$("."+o+"cryptobox_error").fadeIn(400)}),l=!0}).done(function(t){if(cryptobox_update_page(t,o),"payment_received"==t.status&&(i=!0,e||$.post(a+"cryptobox.callback.php",t).fail(function(){alert("Internal Error! Unable to find file cryptobox.callback.php. Please contact the website administrator.")}).done(function(t){"cryptobox_newrecord"!=t&&"cryptobox_updated"!=t&&"cryptobox_nochanges"!=t&&alert("Internal Error! "+t)}),n&&setTimeout(function(){window.location=n},5e3)),!i&&!l){var s=(new Date).getTime();s-r>12e5?($("."+o+"button_wait").hide(),$("."+o+"button_refresh").removeClass("btn-default").addClass("btn-info"),$("."+o+"cryptobox_unpaid .panel").removeClass("panel-info").removeClass("panel-primary").removeClass("panel-warning").removeClass("panel-success").addClass("panel-default").fadeTo("slow",.4,function(){}),$("[data-original-title]").tooltip("disable")):setTimeout(cryptobox_callout,7e3)}})},cryptobox_callout(),!0}function cryptobox_update_page(t,e){logoext="Bitcoin"==t.coinname?"_"+t.texts.language:"","paymentbox"==t.boxtype?$("."+e+"boxlogo").attr("src","https://coins.gourl.io/images/"+t.coinname.toLowerCase()+"/payment"+logoext+".png"):$("."+e+"boxlogo").attr("src","https://coins.gourl.io/images/"+t.coinname.toLowerCase()+"/captcha"+logoext+".png");var a="undefined"==typeof $("."+e+"qrcode_image").attr("data-size")?110:$("."+e+"qrcode_image").attr("data-size");$("."+e+"qrcode_image").attr("src","https://chart.googleapis.com/chart?chs="+a+"x"+a+"&chld=M|0&cht=qr&chl="+t.coinname.toLowerCase()+"%3A"+t.addr+"%3Famount%3D"+t.amount+"&choe=UTF-8"),$.isFunction($.fn.tooltip)&&($("."+e+"wallet_open").attr("data-original-title",t.texts.btn_wallet).attr("data-placement","bottom").attr("data-toggle","tooltip").tooltip(),$("."+e+"qrcode_image").attr("data-original-title",t.texts.qrcode).attr("data-placement","bottom").attr("data-toggle","tooltip").tooltip(),$("."+e+"fees_hint").attr("data-original-title",'<img border="0" width="320" src="https://coins.gourl.io/images/fees.png" />').attr("data-placement","bottom").attr("data-toggle","tooltip").tooltip({html:!0}),$("."+e+"button_wait").attr("data-original-title",t.texts.btn_wait_hint).attr("data-placement","top").attr("data-toggle","tooltip").tooltip()),$("."+e+"paymentcaptcha_amount").text("paymentbox"==t.boxtype?t.texts.payment_amount:t.texts.captcha_amount),$("."+e+"paymentcaptcha_status").text("paymentbox"==t.boxtype?t.texts.payment_status:t.texts.captcha_status);var o="-";"payment_not_received"==t.status?o=t.texts.not_received:"payment_received"==t.status&&(o="paymentbox"==t.boxtype?t.texts.payment_successful:t.texts.captcha_successful),$("."+e+"paymentcaptcha_statustext").text(o),"payment_received"==t.status?($("."+e+"texts_btn_wait_hint").hide(),$("."+e+"button_wait").html("paymentbox"==t.boxtype?t.texts.payment_successful:t.texts.captcha_successful)):$("."+e+"button_wait").html('<i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i> &#160; '+("paymentbox"==t.boxtype?t.texts.payment_wait:t.texts.captcha_wait)),$("."+e+"button_refresh").html('<i class="fa fa-refresh" aria-hidden="true"></i>&#160; '+t.texts.refresh),$("."+e+"paymentcaptcha_title").text("paymentbox"==t.boxtype?t.texts.title:t.coinname),$("."+e+"paymentcaptcha_successful").text("paymentbox"==t.boxtype?t.texts.payment_successful:t.texts.captcha_successful),$("."+e+"paymentcaptcha_date").html(("paymentbox"==t.boxtype?t.texts.received_on:t.texts.captcha_passed)+" <b>"+t.date+"</b>"),$("."+e+"button_details").html('<span class="glyphicon glyphicon-'+("BTC"==t.coinlabel?"bitcoin":"globe")+'" aria-hidden="true"></span>&#160; '+t.texts.btn_res),$("."+e+"button_details").attr("href",t.tx_url).attr("target","_blank"),("fa"==t.texts.language||"ar"==t.texts.language)&&$("."+e+"cryptobox_error, ."+e+"cryptobox_top, ."+e+"cryptobox_unpaid, ."+e+"cryptobox_paid, ."+e+"cryptobox_rawdata").attr("dir","rtl"),$("."+e+"loader").fadeOut(400,function(){$("."+e+"cryptobox_top, ."+e+"cryptobox_rawdata").fadeIn(400),"payment_received"==t.status?($("."+e+"cryptobox_unpaid, ."+e+"boxlogo_unpaid, ."+e+"msg").hide(),$("."+e+"cryptobox_paid, ."+e+"boxlogo_paid").fadeIn(400)):($("."+e+"cryptobox_paid, ."+e+"boxlogo_paid").hide(),$("."+e+"cryptobox_unpaid, ."+e+"boxlogo_unpaid").fadeIn(400)),$("."+e+"msg").delay(7e3).fadeOut(2e3)});var n="";if($.each(t,function(t,a){if("object"==typeof a){var o='<div style="margin-left:50px">';$.each(a,function(a,n){o+="["+a+"] => "+n+"<br>",$("."+e+t+"_"+a).html(n)}),a=o+"</div>"}else t.indexOf("_url")>0?$("."+e+t).attr("href",a):$("."+e+t).html(a);n+="["+t+"] => "+a+"<br>"}),"undefined"!==$("."+e+"texts_intro1b").attr("data-site")&&"undefined"!==$("."+e+"texts_intro1b").attr("data-url")){var r='<a target="_blank" href="'+$("."+e+"texts_intro1b").attr("data-url")+'">'+$("."+e+"texts_intro1b").attr("data-site")+"</a>";$("."+e+"texts_intro1b").html(t.texts.intro1b.replace("___",r))}return $("."+e+"jsondata").html(n),!0}function cryptobox_show(t,e,a,o,n,r,i,l,s,c,p,d,u,m,b,x){"number"!=typeof b&&(b=0),"number"!=typeof x&&(x=0);var y=a.substr(0,a.indexOf("AA"));if(""==y||t!=y||-1==a.indexOf("PUB"))alert("Invalid payment box public_key");else if(0>=o&&0>=n||o>0&&n>0)alert("You can use in payment box options one of variable only: amount or amountUSD. You cannot place values in that two variables together");else if(0!=o&&(o-0!=o||1e-4>o))alert("Invalid payment box amount");else if(0!=n&&(n-0!=n||.01>n))alert("Invalid payment box amountUSD");else if("COOKIE"!=c&&"SESSION"!=c&&"IPADDRESS"!=c&&"MANUAL"!=c)alert("Invalid payment box userFormat value");else if("COOKIE"==c&&""==d)alert("Invalid payment box cookie name");else if("COOKIE"==c&&""==cryptobox_cookie(d))null!=document.getElementById(l).src&&(document.getElementById(l).src="https://gourl.io/images/crypto_cookies.png"),alert("Please enable Cookies in your Browser !");else if("COOKIE"==c&&cryptobox_cookie(d)!=s)alert("Invalid cookie value. It may be you are viewing an older copy of the page that is stored in the website cache. Please contact with website owner, need to disable/turn-off caching for current page");else if(""==p)alert("Invalid orderID");else if(""==r)alert("Invalid period");else if(50!=a.length)alert("Invalid public key");else if(""!=u&&(-1==u.indexOf("DEV")||u.length<20))alert("Invalid webdev_key, leave it empty");else if(""==m)alert("Invalid payment box hash");else{var _="https://coins.gourl.io/b/"+encodeURIComponent(t)+"/c/"+encodeURIComponent(e)+"/p/"+encodeURIComponent(a)+"/a/"+encodeURIComponent(o)+"/au/"+encodeURIComponent(n)+"/pe/"+encodeURIComponent(r.replace(" ","_"))+"/l/"+encodeURIComponent(i)+"/i/"+encodeURIComponent(l)+"/u/"+encodeURIComponent(s)+"/us/"+encodeURIComponent(c)+"/o/"+encodeURIComponent(p)+(u?"/w/"+encodeURIComponent(u):"")+(b>0?"/ws/"+encodeURIComponent(b):"")+(x>0?"/hs/"+encodeURIComponent(x):"")+"/h/"+encodeURIComponent(m)+"/z/"+Math.random(),f=document.getElementById(l);null==f?alert('Cryptobox iframeID HTML with id "'+l+'" not exist!'):f.src=_}return!0}function cryptobox_cookie(t){for(var e=t+"=",a=document.cookie.split(";"),o=0;o<a.length;o++){for(var n=a[o];" "==n.charAt(0);)n=n.substring(1,n.length);if(0==n.indexOf(e))return n.substring(e.length,n.length)}return""}function cryptobox_msghide(t){setTimeout(function(){document.getElementById(t).style.display="none"},15e3)}
     
  