Files `bitcoin-block-hashing.{png,svg,xml}` created by Jack Henschel with [draw.io](https://draw.io).

Licensed under [Creative Commons CC0 1.0](https://creativecommons.org/share-your-work/public-domain/cc0/) (public domain).