$(document).ready(function() {
    var url = $(document.body).data('endpoint') + 'stats?cors=true';

    Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator) {
        var n = this;
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces;
        decSeparator = decSeparator == undefined ? "." : decSeparator;
        thouSeparator = thouSeparator == undefined ? "," : thouSeparator;

        var sign = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;

        return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
    };

    var to2Places = function (value) {
        return Math.round(value * 100) / 100;
    };
    var formatSatoshis = function (value) {
        return (value / 100000000).formatMoney(8) + ' BTC';
    };
    var formatBTC = function (value) {
        return value.formatMoney(8) + ' BTC';
    };
    var formatUSD = function (value) {
        return '$' + value.formatMoney(2);
    };
    var formatPercent = function (value) {
        return to2Places(value * 100.0) + '%';
    };
    var formatNumber = function (value) {
      return value.formatMoney(0);
    }

    var transformers = {
        'minutes_between_blocks': function (value) { return to2Places(value); },
        'n_btc_mined': formatSatoshis,
        'total_fees_btc': formatSatoshis,
        'n_tx': formatNumber,
        'total_btc_sent': formatSatoshis,
        'estimated_btc_sent': formatSatoshis,
        'estimated_transaction_volume_usd': formatUSD,
        'market_price_usd': formatUSD,
        'miners_revenue_usd': formatUSD,
        'miners_revenue_percent_fees': formatPercent,
        'miners_revenue_percent_volume': formatPercent,
        'miners_cost_per_tx': formatUSD,
        'trade_volume_btc': formatBTC,
        'trade_volume_usd': formatUSD,
        'difficulty': function (value) { return value.formatMoney(0); },
        'hash_rate': function (value) { return value.formatMoney(0) + ' GH/s'; }
    };

    function update () {
        ajaxCall($(document.body).data('explorer') + '/ticker?cors=true', function(data) {
            updateStats(data['USD']['last']);
        });
    }

    function updateStats(price) {
        ajaxCall(url, function (stats) {
            stats['market_price_usd'] = price;
            stats['miners_revenue_percent_fees'] = (stats['total_fees_btc'] / 100000000.0 * stats['market_price_usd']) / stats['miners_revenue_usd'];
            stats['miners_revenue_percent_volume'] = stats['miners_revenue_usd'] / stats['estimated_transaction_volume_usd'];
            stats['miners_cost_per_tx'] = (stats['n_btc_mined'] + stats['total_fees_btc']) / stats['n_tx'] * stats['market_price_usd'] / 100000000.0;
            for (var property in stats) {
                if (stats.hasOwnProperty(property)) {
                    if (transformers.hasOwnProperty(property)) {
                        $("#" + property).text(transformers[property](stats[property]));
                    } else {
                        $("#" + property).text(stats[property]);
                    }
                }
            }}, function () {
                $("#statserror").show();
                $("#statstable").hide();
            }
        );
    }

    update();
    setInterval(update, 30 * 60 * 1000);
});
