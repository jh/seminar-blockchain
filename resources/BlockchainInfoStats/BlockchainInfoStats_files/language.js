$(document).ready(function() {
    try {
        var language_links = $('#languages_select').find('a');
        language_links.click(function(e) {
            e.preventDefault();

            var new_code = $(this).data('code');

            SetCookie('clang', new_code);

            var path = window.location.pathname;

            var found = false;
            language_links.each(function() {
                var code = $(this).data('code');
                if (path.indexOf('/'+code) == 0) {
                    path = path.replace('/'+code, '/'+new_code);
                    found = true;
                    return false;
                }
            });

            if (!found) {
                path = '/' + new_code + path;
            }

            window.location.href = path;
        });
    } catch (e) {}
});