# Paper: Quantitative Aspects of the Blockchain: Proof Of Work, its Energy Usage and Alternative Consensus Mechanisms

Written treatise based on the presentation.

## Formalities

i7 Template: http://www7.cs.fau.de/en/wp-content/uploads/sites/3/2017/01/i7-template.tar_.gz

## General Rules and Hints from i7:

### Structure of the document
    The main document should be organized as follows. The ratio between the main sections (2.-4.) is 1/3 to 1/3 to 1/3! Regarding the size of the thesis, a rough measure might be 60-80 pages for a bachelor thesis and 80-90 for a master’s thesis.

#### Abstract / Kurzfassung: each about 1/2 page
* [How to write an abstract](http://www7.cs.fau.de/en/teaching-overview/student-theses/writing-your-thesis/)
* Motivation (Why do we care?)
* Problem statement (What problem are we trying to solve?)
* Approach (How did we go about it)
* Results (What’s the answer?)
* Conclusion (What are the implications of the answer?)

#### 1. Intoduction (general motivation for your work, context and goals): 1-2 pages
* Context: make sure to link where your work fits in
* Problem: gap in knowledge, too expensive, too slow, a deficiency, superseded technology
* Strategy: the way you will address the problem

#### 2. Fundamentals / environment and related work: 1/3
* comment on employed hardware and software
* describe methods and techniques that build the basis of your work
* review related work(!)

#### 3. Developed architecture / system design / implementation: 1/3
* start with a theoretical approach
* describe the developed system/algorithm/method from a high-level point of view
* go ahead in presenting your developments in more detail

#### 4. Measurement results / analysis / discussion: 1/3
* whatever you have done, you must comment it, compare it to other systems, evaluate it
* usually, adequate graphs help to show the benefits of your approach
* caution: each result/graph must be discussed! what’s the reason for this peak or why have you ovserved this effect

#### 5. Conclusion: 1 page
* summarize again what your paper did, but now emphasize more the results, and comparisons
* write conclusions that can be drawn from the results found and the discussion presented in the paper
* future work (be very brief, explain what, but not much how)

#### References
* all papers and articles used in the thesis must be cited (and each reference must be used in the thesis!)
* a rough number is 20 references for a bachelor thesis and 30-40 for a master’s thesis
* avoid to cite web sites
* We highly recommend to use Endnote or BibTeX for creating the references and citings
* Further information: [IEEE Rules](http://www7.cs.fau.de/en/teaching-overview/student-theses/writing-your-thesis/), [BibTeX](http://www7.cs.fau.de/en/teaching-overview/student-theses/writing-your-thesis/)

### Writing style
* Avoid passive voice, active voice is easier to read. There is nothing wrong saying I (or we) did it
* Avoid negative sentences: write in a positive (affirmative) voice, they are easier to understand.
* Always use vector graphics for figures (PDF, EPS, …)

### Last minute checks
* Did I spell out the main points of the interpretation of results?
* Are all equations, figures, tables numbered?
* Do all graphs, tables, diagrams have descriptive captions?
* Are all axes and scale carefully chosen to show the relevant effects?
* Are all axes labelled? Do the labals include the measurement units?
* Are citations in the caption (if a graph is borrowed)?

Source: http://www7.cs.fau.de/en/teaching-overview/student-theses/writing-your-thesis/
